package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameRegular() {
		boolean isValidName = LoginValidator.isValidLoginName("deep988");
		assertTrue( "Invalid LoginName", isValidName);	
	}
	
	@Test
	public void testIsValidLoginNameException() {
		boolean isValidName = LoginValidator.isValidLoginName("98deep8");
		assertFalse( "Invalid LoginName", isValidName);	
	}
	
	@Test
	public void testIsValidLoginNameBoundaryIn() {
		boolean isValidName = LoginValidator.isValidLoginName("de98ep8");
		assertTrue( "Invalid LoginName", isValidName);
	}
	@Test
	public void testIsValidLoginNameBoundaryOut() {
		boolean isValidName = LoginValidator.isValidLoginName("9deep88");
		assertFalse( "Invalid LoginName", isValidName);
	}
	
	@Test
	public void testIsValidLoginNameLengthRegular() {
		boolean isValidName = LoginValidator.isValidLoginName("deep988navadiya");
		assertTrue( "Invalid LoginName", isValidName);	
	}
	
	@Test
	public void testIsValidLoginNameLengthException() {
		boolean isValidName = LoginValidator.isValidLoginName("de");
		assertFalse( "Invalid LoginName", isValidName);	
	}
	
	@Test
	public void testIsValidLoginNameLengthBoundaryIn() {
		boolean isValidName = LoginValidator.isValidLoginName("de98ep");
		assertTrue( "Invalid LoginName", isValidName);
	}
	@Test
	public void testIsValidLoginNameLengthBoundaryOut() {
		boolean isValidName = LoginValidator.isValidLoginName("deep8");
		assertFalse( "Invalid LoginName", isValidName);
	}
}
